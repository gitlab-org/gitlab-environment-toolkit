---
# Cloud Provider specific
kubeconfig_setup: false

gcp_backups_service_account_key_file: ""
gcp_backups_service_account_key_json: "{{ (lookup('file', gcp_backups_service_account_key_file) | string) if gcp_backups_service_account_key_file != '' }}"
gcp_zone: ""
gcp_gke_location: "{{ gcp_zone }}"
gcp_gke_workload_identity_setup: false
gcp_service_account_prefix: "gl"

aws_partition: ""
aws_account: ""
aws_profile: ""

aws_eks_metrics_server_setup: true
aws_eks_metrics_server_version: ""
aws_eks_metrics_server_manifest_url: "https://github.com/kubernetes-sigs/metrics-server/releases/{{ 'latest/download' if aws_eks_metrics_server_version == '' else 'download/' + aws_eks_metrics_server_version }}/components.yaml"

# Operator settings
gitlab_operator_cert_manager_version: "{{ lookup('env','CERT_MANAGER_VERSION') | default('v1.16.2', true) }}" # https://cert-manager.io/docs/installation/
gitlab_operator_cert_manager_manifest_url: "https://github.com/cert-manager/cert-manager/releases/download/{{ gitlab_operator_cert_manager_version }}/cert-manager.yaml"

# Charts Settings
gitlab_charts_repo_name: 'gitlab'
gitlab_charts_repo_url: "https://charts.gitlab.io/"
gitlab_charts_ref: "{{ gitlab_charts_repo_name }}/gitlab"
gitlab_charts_direct_install: "{{ offline_setup or gitlab_charts_ref.startswith('/') or gitlab_charts_ref.endswith('.tgz') }}"

gitlab_charts_show_values: false

## Webservice
gitlab_charts_webservice_requests_memory_gb: 5
gitlab_charts_webservice_limits_memory_gb: 7
gitlab_charts_webservice_requests_cpu: 4

gitlab_charts_webservice_min_replicas_scaler: 0.75
gitlab_charts_webservice_max_unavailable_scaler: 0.1
gitlab_charts_webservice_max_replicas: ""
gitlab_charts_webservice_min_replicas: ""
gitlab_charts_webservice_max_unavailable: ""

### Default Replica / Pod counts as given by Reference Architectures, calculated by equivalent Linux Package (Omnibus) Gitaly CPU counts if available
webservice_default_replica_counts:
  '192': 77 # 1000 RPS / 50k User - 308 Puma workers (80*4)
  '96': 35 # 500 RPS / 25k User - 140 Puma workers (35*4)
  '48': 20 # 200 RPS / 10k User - 80 Puma workers (20*4)
  '24': 9 # 100 RPS / 5k User - 36 Puma workers (9*4)
  '12': 4 # 40 RPS / 3k User - 16 Puma workers (4*4)
  '4': 3 # 20 RPS / 2k User - 12 Puma workers (3*4)

## Sidekiq
gitlab_charts_sidekiq_requests_memory_gb: 2
gitlab_charts_sidekiq_limits_memory_gb: 4
gitlab_charts_sidekiq_requests_cpu: 0.9
gitlab_charts_sidekiq_min_replicas_scaler: 0.75
gitlab_charts_sidekiq_max_unavailable_scaler: 0.1
gitlab_charts_sidekiq_max_replicas: ""
gitlab_charts_sidekiq_min_replicas: ""
gitlab_charts_sidekiq_max_unavailable: ""

### Default Replica / Pod counts as given by Reference Architectures, calculated by equivalent Linux Package (Omnibus) Gitaly CPU counts if available
sidekiq_default_replica_counts:
  '192': 14
  '96': 14
  '48': 14
  '24': 8
  '12': 8
  '4': 4

## Gitaly
## EXPERIMENTAL. Deploying Gitaly on Kubernetes is not supported in Production - https://gitlab.com/groups/gitlab-org/-/epics/6127
gitlab_charts_gitaly_setup: false

gitlab_charts_gitaly_requests_memory_gb: ''
gitlab_charts_gitaly_limits_memory_gb: "{{ gitlab_charts_gitaly_requests_memory_gb }}"
gitlab_charts_gitaly_requests_cpu: ''
gitlab_charts_gitaly_storage_size: '50Gi'
gitlab_charts_gitaly_storage_class: ''
gitlab_charts_gitaly_storage_names: ['default']

# Gitaly cgroups defaults
## Memory - Gitaly cgroups are allocated the larger of: 90% of total memory, or total memory minus 6GB. This ensures at least 10% or 6GB (whichever is larger) is available for other processes not covered in cgroups. (gitaly, gitaly-logger, etc...)
gitlab_charts_gitaly_cgroups_memory_scaler: 0.9
gitlab_charts_gitaly_cgroups_memory_bytes: "{{ ((([gitlab_charts_gitaly_limits_memory_gb * gitlab_charts_gitaly_cgroups_memory_scaler, gitlab_charts_gitaly_limits_memory_gb - 6] | max) * 1000 * 1000 * 1000) | round | int) if gitlab_charts_gitaly_limits_memory_gb != '' else '' }}"

## Specific overrides for Cluster Autoscaler image tag if default .0 image has issues
cluster_autoscaler_image_tag:
  '1.26': 'v1.26.2'

## Kube Prometheus Stack
kube_prometheus_stack_charts_namespace: monitoring
kube_prometheus_stack_charts_app_version: "{{ lookup('env','KUBE_PROMETHEUS_STACK_CHARTS_APP_VERSION') | default('v0.79.2', true) }}"

kube_prometheus_stack_charts_storage_size: 100Gi
kube_prometheus_stack_charts_storage_class: ""

kube_prometheus_stack_charts_prometheus_scrape_config_setup: true
kube_prometheus_stack_charts_custom_scrape_config: []

consul_charts_namespace: consul
consul_charts_app_version: "{{ lookup('env','CONSUL_CHARTS_APP_VERSION') | default('1.18.2', true) }}"
